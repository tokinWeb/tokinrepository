import React from 'react'
import '../css/listaBandas.css';

const Banda = ({id, uuid, nombre, genero, correo, tel_movil, tipo, password, estatus, imagen, direccion_id, fecha_registro, fecha_act, onClick= () => {} }) =>
	<div className="container">
        <div className="banda">
		<a onClick={() => onClick({id})}>
            <img alt={nombre} src={imagen} />
        </a>
        	<span className="name">{nombre}</span>
        </div>
    </div>
    
export default Banda