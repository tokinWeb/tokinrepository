import React from 'react'
import band from '../images/logo-la-banda.png'
import { Link } from 'react-router-dom';
import '../css/mandarSolicitud.css';

const BandaDetalle = ({id, uuid, nombre, genero, correo, tel_movil, tipo, password, estatus, imagen, direccion_id, fecha_registro, fecha_act, onClick= () => {} }) =>
    <div className="band">
    	<div id="datos">
	        <span className="tel">Teléfono: {tel_movil}</span>
	        <span className="mail">Correo: {correo}</span>
	        <span className="gender">Género: {genero}</span>
	    </div>
	      <div id="buttons1">
  		    <button  id="boton" className="button"> <Link to ="/mandarSolicitud" style={{color:"white"}} >Solicitar Tokin</Link></button>
  		    <button id="boton" className="button1"><Link to ="/controlEvaluaciones" style={{color:"white"}} >Evaluaciones</Link></button>
        </div>
        <div id="buttons2">
	        <button id="boton" className="button"><Link to ="/controlChat" style={{color:"white"}} >Chatear</Link></button>
	        <button id="boton" className="button1"><Link to ="/evaluacion" style={{color:"white"}} >Evaluar</Link></button>
        </div>
    </div>

BandaDetalle.propTypes = {
  name:React.PropTypes.string.isRequired,
  tel_movil:React.PropTypes.string.isRequired,
  correo:React.PropTypes.string.isRequired,
  imagen:React.PropTypes.string.isRequired,
  genero:React.PropTypes.string.isRequired
}

BandaDetalle.defaultProps = {
  name:"[nombre]",
  character:"[personaje]",
  photo:"[photo]"
}

export default BandaDetalle;