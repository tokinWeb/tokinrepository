import React from 'react'
import band from '../images/logo-la-banda.png'
import { Link } from 'react-router-dom';
import '../css/mandarSolicitud.css';

const BarDetalle = ({id, uuid, nombre, genero, correo, tel_movil, tipo, password, estatus, imagen, direccion_id, fecha_registro, fecha_act, onClick= () => {} }) =>
    <div className="band">
    	<div id="datos">
          <span className="tel">Teléfono: {tel_movil}</span>
          <span className="mail">Correo: {correo}</span>
          <span className="gender">Género: {genero}</span>
	    </div>
	    <div id="buttons1">
        <button  id="boton" className="button"> <Link to ="/mandarSolicitud" style={{color:"white"}} >Solicitar Tokin</Link></button>
        <button id="boton" className="button1"><Link to ="/controlEvaluaciones" style={{color:"white"}} >Evaluaciones</Link></button>
        </div>
        <div id="buttons2">
          <button id="boton" className="button"><Link to ="/controlChat" style={{color:"white"}} >Chatear</Link></button>
          <button id="boton" className="button1"><Link to ="/evaluacion" style={{color:"white"}} >Evaluar</Link></button>
        </div>
    </div>

BarDetalle.propTypes = {
  name:React.PropTypes.string.isRequired,
  address:React.PropTypes.string.isRequired,
  tel:React.PropTypes.string.isRequired,
  workingH:React.PropTypes.string.isRequired,
  photo:React.PropTypes.string.isRequired,
  stars:React.PropTypes.string.isRequired
}

BarDetalle.defaultProps = {
  name:"[nombre]",
  character:"[personaje]",
  photo:"[photo]"
}

export default BarDetalle;