import React, { Component } from 'react';
import ListaBandas from './ListaBandas.js';
import Banda from './Banda';
import '../css/mensajes.css';
import BandaDetalle from './BandaDetalle.js'
import axios from 'axios';

class ControlBandas extends Component {

    constructor( props) {
        super( props);
        this.state = { bandas: [],
                    detalles:[],
                    filtrado:''

        };

    this.onClick = this.onClick.bind(this);
    this.onRegresar = this.onRegresar.bind(this);
    this.state.filtrado=this.state.bandas;

    }

    componentWillMount(){


            axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/userB")
            .then(res => {
                this.setState({filtrado: res.data});
                this.setState({bandas: res.data});

            })          
        
    }


  onClick(id) {   
        const filtrado = this.state.filtrado.filter(banda => banda.id === id);


        localStorage.setItem('idm', id);


        this.setState({
            filtrado: filtrado,
            detalles:filtrado
        })


  }

  onRegresar(id) {   

        console.log(localStorage.getItem('tipo'));
        this.setState({
            filtrado: this.state.bandas,
            detalles: []
        })

        console.log("hola")
        console.log(this.state.detalles)
  }

    render() {
       return (
            <div>
                <button onClick={this.onRegresar}> Ver Todos</button>
                <ListaBandas bandas={this.state.filtrado} onClick={this.onClick}/>
                <div>
                    <ul>
                        {this.state.detalles.map((banda, i) =>
                            <BandaDetalle key={i} {...banda} />
                        )}
                    </ul>
                </div>
            </div>
        );
    }
}

export default ControlBandas;