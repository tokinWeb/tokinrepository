import React, { Component } from 'react';
import ListaBares from './ListaBares.js';
import Bar from './Bar';
import '../css/mensajes.css';
import BarDetalle from './BarDetalle.js'
import axios from 'axios';
import Evaluacion from './Evaluacion.js'

class ControlBares extends Component {

    constructor( props) {
        super( props);
        this.state = { bares: [],
                    detalles:[],
                    filtrado:''
        };

    this.onClick = this.onClick.bind(this);
    this.onRegresar = this.onRegresar.bind(this);
    this.state.filtrado=this.state.bares;

    }

    componentWillMount(){

        axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/userR")
            .then(res => {
                this.setState({filtrado: res.data});
                this.setState({bares: res.data});

            })
    }

  onClick(id) {   
        const filtrado = this.state.filtrado.filter(bar => bar.id === id);

        console.log("filtrado " + this.state.filtrado.filter(bar => bar.id === id))
        this.setState({
            filtrado: filtrado,
            detalles:filtrado,
        })
        console.log(id);
        localStorage.setItem('idr', id);

        id:this.state.id

        console.log("id " + id);

        
  }

  onRegresar(id) {   

        this.setState({
            filtrado: this.state.bares,
            detalles: []
        })

        console.log("hola")
        console.log("detalles: " + this.state.detalles)


  }

    render() {
       return (
            <div>
                <button onClick={this.onRegresar}> Ver Todos</button>
                <ListaBares bares={this.state.filtrado} onClick={this.onClick}/>
                <div>
                    <ul>
                        {this.state.detalles.map((bar, i) =>
                            <BarDetalle key={i} {...bar} />
                        )}
                    </ul>
                </div>
            </div>
        );
    }
}

export default ControlBares;