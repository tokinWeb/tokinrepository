import React, { Component } from 'react';
import ListaMensajesChat from './ListaMensajesChat.js';
import MensajeIndividual from './MensajeIndividual.js';
import '../css/mensajeChat.css';
import axios from 'axios';
import MensajeChat from './MensajeChat.js'

class ControlChat extends Component {

    constructor( props) {
        super( props);
        this.state = { 
            mensajesChat: [/*
                        {
                            usuario_id:23,
                            usuario_id_e:24,
                            texto:"Hola, recibimos tu solicitud para tocar en La Clandestina, estamos muy interesados, ¿qué tipo de música tocan?"
                        },
                        {
                            usuario_id:24,
                            usuario_id_e:23,
                            texto:"Hola! Tocamos música progresiva, creemos que daríamos un buen ambiente a tu bar en las horas en que nos contrates, tus clientes nos amaran."
                        }*/
            ]
        };

    }

    componentWillMount(){

        axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/mensajeA/eventAll")
            .then(res => {
                this.setState({mensajesChat: res.data});
            })
    }

    render() {
       return (
            <div>
                <ListaMensajesChat mensajesChat={this.state.mensajesChat} />
            </div>
        );
    }
}

export default ControlChat;