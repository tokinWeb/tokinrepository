import React, { Component } from 'react';
import ListaEvaluaciones from './ListaEvaluaciones.js';
import '../css/mensajes.css';
import axios from 'axios';

class ControlEvaluaciones extends Component {

    constructor( props) {
        super( props);
        this.state = { 
            evaluaciones: []
        };

    }

    componentWillMount(){
         axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/eventAll")
           .then(res => {
               this.setState({evaluaciones: res.data});
           })
     }

    render() {
       return (
            <div>
                <ListaEvaluaciones evaluaciones={this.state.evaluaciones} />
            </div>
        );
    }
}

export default ControlEvaluaciones;