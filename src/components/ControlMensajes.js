import React, { Component } from 'react';
import ListaMensajes from './ListaMensajes.js';
import '../css/mensajes.css';
import axios from 'axios';

class ControlMensajes extends Component {

    constructor( props) {
        super( props);
        this.state = { mensajes: [
                        {
                            id:1,
                            name_e:"La clandestina",
                            date:"Lunes, 23 Octubre, 21:00"
                        },
                        {
                            id:2,
                            name_e:"Salón Pata Negra",
                            date:"Miercoles, 25 Octubre, 19:00"
                        }
                    ]
        };

    }

    render() {
       return (
            <div>
                <ListaMensajes mensajes={this.state.mensajes} />
            </div>
        );
    }
}

export default ControlMensajes;