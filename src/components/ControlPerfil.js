import React, { Component } from 'react';
import ListaMensajes from './ListaMensajes.js';
import '../css/mensajes.css';
import axios from 'axios';
import Perfil from './Perfil.js'

class ControlPerfil extends Component {

    constructor( props) {
        super( props);
        this.state = { 
                          imagen:"http://4.bp.blogspot.com/_szMYSbG0Z3I/TEupscI9SiI/AAAAAAAAAsw/JNwy9sSCXjE/s1600/aloha+from+hell+.+(1).jpg",
                          nombre:"Sublime",
                          tel_movil:"55-67-56-45-90",
                          correo:"sublime@gmail.com",
                          genero:"rock"
                        
                
        };

    }

    componentWillMount(){

       axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/user")
            .then(res => {
                this.setState({perfils: res.data});

            })



    }

    render() {
       return (
            <div>
                <Perfil imagen={this.state.imagen} 
                        nombre={this.state.nombre}
                        tel_movil={this.state.tel_movil}
                        correo={this.state.correo}
                        genero={this.state.genero}
                />
            </div>
        );
    }
}

export default ControlPerfil;