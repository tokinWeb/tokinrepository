import React, { Component } from 'react';
import ListaSolicitudes from './ListaSolicitudes.js'
import '../css/solicitudes.css';
import axios from 'axios'

class ControlSolicitudes extends Component {

    constructor( props) {
        super( props);
        this.state = { solicitudes: [],
                        names: []
        };

        this.rechazar=this.rechazar.bind(this);
        this.aceptar=this.aceptar.bind(this);
    }


    aceptar(id) {   
         axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).put("http://localhost:8080/v1/event/a/"+id)
            .then(res => {
                //console.log(res.data.principal);
            })
        const solicitudes = this.state.solicitudes.filter(solicitud => solicitud.uuid != id)
        this.setState({solicitudes})

        console.log(localStorage.getItem('id'))
                console.log(localStorage.getItem('idm'))


    }

    rechazar(id) {   
         axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).put("http://localhost:8080/v1/event/r/"+id)
            .then(res => {
                //console.log(res.data.principal);
            })

        const solicitudes = this.state.solicitudes.filter(solicitud => solicitud.uuid != id)
        this.setState({solicitudes})
    }




    componentWillMount(){

         axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/eventsAll/" + localStorage.getItem('id'))
            .then(res => {
                this.setState({solicitudes: res.data});


                for(let x=0; x<this.state.solicitudes.length; x++){
                     axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/userd/" + this.state.solicitudes[x].usuario_id_requester)
                    .then(res2 => {
                        const names=[...this.state.names, res2.data.nombre];
                        this.setState({names});

                   })
                }
            })



    }

    render() {
       return (

            <div>
                 <ListaSolicitudes  name={this.state.names} solicitudes={this.state.solicitudes} aceptar={this.aceptar} rechazar={this.rechazar}/>
            </div>
        );
    }
}

export default ControlSolicitudes;