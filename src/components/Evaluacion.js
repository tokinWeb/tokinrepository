import React from 'react'
import { PropTypes } from 'react'
import { Component } from 'react'
import { Link } from 'react-router-dom';
import band from '../images/logo-la-banda.png'
import PageTemplate from './PageTemplate'
import axios from 'axios';
import '../css/evaluacion.css';
import Rating from './Rating.js'


class Evaluacion extends Component {
    constructor(props){
        super(props);


        this.state = { id: localStorage.getItem('idm')};

        this.submit = this.submit.bind(this)

        estrellas:React.PropTypes.int
        comentario:React.PropTypes.string
        usuario_id:React.PropTypes.int
        usuario_id_e:React.PropTypes.int

    }

    static propTypes = {
        onNewEval:PropTypes.func.isRequired
    };

    static defaultProps = {
        onNewEval: () => {}
    }    

    submit(e){

        var estrellas1 = document.getElementById('star-1');
        var estrellas2 = document.getElementById('star-2');
        var estrellas3 = document.getElementById('star-3');
        var estrellas4 = document.getElementById('star-4');
        var estrellas5 = document.getElementById('star-5');

        var starValue = -1;

        if ( estrellas1.checked ) {
            console.log("1 -SELECTIONADO");
            starValue = 1;
        }

        if ( estrellas2.checked ) {
            console.log("2 -SELECTIONADO");
            starValue = 2;
        }

        if ( estrellas3.checked ) {
            console.log("3 -SELECTIONADO");
            starValue = 3;
        }

        if ( estrellas4.checked ) {
            console.log("4 -SELECTIONADO");
            starValue = 4;
        }

        if ( estrellas5.checked ) {
            console.log("5 -SELECTIONADO");
            starValue = 5;
        }

        console.log(estrellas5.checked);

        //let starsValue = _estrellas;

        e.preventDefault();
        const {starsValue, _comentario, _usuario_id, _usuario_id_e} = this.refs;


        this.props.onNewEval(starsValue.value, _comentario.value, _usuario_id.value, localStorage.getItem('idr'));

        console.log(starValue + " " + _comentario.value + " " + _usuario_id.value + " " + localStorage.getItem('idr'));
        console.log(localStorage.getItem('idr'))

         axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).post('http://localhost:8080/v1/eval', {

        estrellas:starValue,
        comentario:_comentario.value,
        usuario_id:localStorage.getItem('idm'),
        usuario_id_e:localStorage.getItem('idr')

    })

        .then(function (response) {
        console.log(response);
         })
        .catch(function (error) {
        console.log(error);
        });

    }


    render(){
        return (

    <div className="eval">
		<form onSubmit={this.submit}>
		<Rating ref="starsValue"/>
            <div>                 
                <label for="usuario_id">
                    <input type="hidden" ref="_usuario_id" ></input> 
                </label>  
            </div>
            <div>                 
                <label for="usuario_id_e">
                    <input type="hidden" ref="_usuario_id_e" ></input> 
                </label>  
            </div>
            <textarea type="text" ref="_comentario" ></textarea> 
            
            <div id="buttonEval">
            	<button className="evaluar" type="submit" id="submit" >Evaluar</button> 
    		</div>

    </form>

	</div>

        )
    }

}

export default Evaluacion;
