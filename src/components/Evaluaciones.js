import React from 'react'
import '../css/mensajes.css';
import Rating from './Rating.js'

const Evaluaciones = ({id, name_e, name_r, comentario, estrellas, onRemove = () => {} }) =>
    <li>
        <span className="name">{name_e}</span>
        <span className="texto"><Rating/></span>
        <span className="comentario">{comentario}</span>
    </li>;
export default Evaluaciones