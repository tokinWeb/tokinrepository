import React from 'react'
import Banda from './Banda.js'

const ListaBandas = ({bandas, onClick= () => {} }) =>
    <div className="list">
        <ul>
            {bandas.map((banda, i) =>
                <Banda onClick={() => onClick(bandas[i].id)} key={i} {...banda} />
            )}
        </ul>
    </div>;

export default ListaBandas