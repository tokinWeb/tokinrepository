import React from 'react'
import Bar from './Bar.js'

const ListaBares = ({bares,onClick= () => {} }) =>
    <div className="list">
        <ul>
            {bares.map((bar, i) =>
                <Bar onClick={() => onClick(bares[i].id)} key={i} {...bar} />
            )}
        </ul>
    </div>;

export default ListaBares