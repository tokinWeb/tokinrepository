import React from 'react'
import Concierto from './Concierto.js'

const ListaConciertos = ({namereq, nameace, conciertos, fecha= () => {} }) =>
    <div className="list">
        <ul>
            {conciertos.map((concierto, i) =>
                <Concierto namereq={namereq[i]} nameace={nameace[i]} key={i} {...concierto} />
            )}
        </ul>
    </div>;

export default ListaConciertos