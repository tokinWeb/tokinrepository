import React from 'react'
import Evaluaciones from './Evaluaciones.js'

const ListaEvaluaciones = ({evaluaciones, onRemove = () => {} }) =>
    <div className="list">
        <ul>
            {evaluaciones.map((evaluacion, i) =>
                <Evaluaciones key={i} {...evaluacion} />
            )}
        </ul>
    </div>;

export default ListaEvaluaciones