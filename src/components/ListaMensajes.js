import React from 'react'
import Mensaje from './Mensaje.js'

const ListaMensajes = ({mensajes, onRemove = () => {} }) =>
    <div className="list">
        <h2>Mis mensajes</h2>
        <ul>
            {mensajes.map((mensaje, i) =>
                <Mensaje key={i} {...mensaje} />
            )}
        </ul>
    </div>;

export default ListaMensajes