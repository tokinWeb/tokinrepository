import React from 'react'
import Solicitud from './Solicitud.js'


const ListaSolicitudes = ({name, solicitudes, aceptar, rechazar= () => {}  }) =>
   
    <div className="list">
        <h2>Solicitudes</h2>
        <ul>
            {solicitudes.map((solicitud, i) =>

                <Solicitud name={name[i]} aceptar={() => aceptar(solicitudes[i].uuid)} rechazar={() => rechazar(solicitudes[i].uuid)} key={i} {...solicitud} />
            )}
        </ul>
    </div>;

export default ListaSolicitudes;