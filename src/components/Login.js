import { PropTypes, Component } from 'react'
import React from 'react'
import PageTemplate from './PageTemplate'
import '../css/login.css';
import { withCookies } from 'react-cookie';
import axios from 'axios';


class Login extends Component {
    constructor(props){
        super(props);
        this.submit = this.submit.bind(this)
        this.state = {usuario:{}};
    }

    static propTypes = {
        onLogin:PropTypes.func.isRequired
    };

   

    /**

componentWillMount() {
    const {cookies} = this.props;
        let code = getParameterByName("code");
        if(localStorage.getItem("accesstoken") === null && code === null){
            window.location.href = "http://localhost:8081/oauth/authorize?response_type=code&client_id=oauthserver&redirect_uri=http://localhost:3000";
        } else if(code !== null){
             console.log("Entre");
            axios.create({
                headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
            }).post("http://localhost:8081/oauth/token","grant_type=authorization_code&client_id=oauthserver&redirect_uri=http://localhost:3000&code="+code)
                .then(res => {
                    console.log(res);
                    cookies.set("accesstoken",res.data.access_token);
                    localStorage.setItem("accesstoken",res.data.access_token);
                    this.getUserData();
                     console.log(res.data.access_token);
                }).catch(() => {
                    console.log("Error");
                window.location.href = "/"
                })
        } else if(localStorage.getItem("accesstoken") !== null){
            this.getUserData();
            console.log(this.state.user.nombre);
        }

    }

getUserData(){
        // se obtienen los datos del usuario del servidor de oauth
        axios.create({
            headers:{'Authorization':'Bearer '+localStorage.getItem("accesstoken")}
        }).get("http://localhost:8081/user")
            .then(res => {
                console.log(res.data.principal);
                this.setState({user:res.data.principal});
                this.getUsersFromAPI();
                console.log(this.state.user);
            })
            .catch(() => {
                localStorage.removeItem("accesstoken");
            })
    }

   
getUserData(){
        const { cookies } = this.props;
        axios.create({
            headers:{'Authorization':'Bearer '+cookies.get('accesstoken')}
        }).get("http://localhost:8081/user")
            .then(res => {
                console.log(res.data.principal);
                this.setState({user:res.data.principal});
                localStorage.setItem("nombre", res.data.principal.nombre);
            })
            .catch(() => {
                cookies.remove("accesstoken");
            })
    }
    
    componentWillMount() {
        const { cookies } = this.props;
        let code = getParameterByName("code");
        if(cookies.get('accesstoken') !== undefined){
            this.getUserData();
            console.log(this.state.user.nombre);
        }else if(code !== null){
            axios.create({
                headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
            }).post("http://localhost:8081/oauth/token","grant_type=authorization_code&client_id=oauthserver&redirect_uri=http://localhost:3000/login/&code="+code)
                .then(res => {
                    console.log(res);
                    cookies.set("accesstoken",res.data.access_token);
                    console.log(this.state.user.nombre);
                    this.getUserData();
                }).catch(() => {
                    console.log("Error");
                window.location.href = "/"
                })
            
        } else if(code === null && cookies.get('accesstoken') === undefined){
            window.location.href = "http://localhost:8081/oauth/authorize?response_type=code&client_id=oauthserver&redirect_uri=http://localhost:3000";
        } 


    }

    getUserData(){
        const { cookies } = this.props;
        axios.create({
            headers:{'Authorization':'Bearer '+cookies.get('accesstoken')}
        }).get("http://localhost:8081/user")
            .then(res => {
                console.log(res.data.principal);
                this.setState({user:res.data.principal});
                console.log(this.state.user.nombre);
            })
            .catch(() => {
                cookies.remove("accesstoken");
            })
    }**/
    submit(e){
        e.preventDefault();
        const {_mail, _password} = this.refs;
        this.props.onNewSerie(_mail.value, _password.value);
        _mail.value = '';
        _password.value = '';
        _mail.focus();
    }

    render(){
        return (
            <div id="contact-form">
                <div>
                    <h1 id="datos">Ingresa tus datos</h1> 
                </div>
                <p id="failure">Oopsie...message not sent.</p>  
                <p id="success">Your message was sent successfully. Thank you!</p>
                <form method="post" action="/">
                <div>                 
                    <label for="message">
                        <span class="required">{this.state.user.nombre}:</span> 
                         <input type="text" id="message" name="message" tabindex="5" required="required"></input> 

                    </label>  
                </div>
                <div>                 
                    <label for="message">
                        <span class="required">Contraseña:</span> 
                        <input type="password" id="message" name="message" tabindex="5" required="required"></input> 
                    </label>  
                </div>
                <div>                  
                    <button name="submit" type="submit" id="submit" >Ya déjame entrar</button> 
                </div>
                </form>
            </div> 
        )
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
export default withCookies(Login);
