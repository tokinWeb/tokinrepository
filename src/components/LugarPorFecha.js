import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { PropTypes } from 'react'
import { Component } from 'react'
import PageTemplate from './PageTemplate'
import ListaConciertos from './ListaConciertos.js';
import axios from 'axios'
import '../css/react-datepicker.css';
import '../css/buscarFecha.css';

class LugarPorFecha extends React.Component {
  constructor (props) {
    super(props)
    this.state = {

      filtrado: [],
      conciertos: [],
      namereqs: [],
      nameaces: [],
      startDate: moment().subtract(1, "days")
    };
    this.handleChange = this.handleChange.bind(this);


  }

      componentWillMount(){

             axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/eventsAllAll/" + localStorage.getItem('id'))
            .then(res => {
                this.setState({conciertos: res.data});

                 for(let x=0; x<this.state.conciertos.length; x++){
                    axios.get("http://localhost:8080/v1/userd/" + this.state.conciertos[x].usuario_id_requester)
                    .then(res2 => {
                        const namereqs=[...this.state.namereqs, res2.data.nombre];
                        this.setState({namereqs});

                   })
                }


                 for(let x=0; x<this.state.conciertos.length; x++){
                    axios.get("http://localhost:8080/v1/userd/" + this.state.conciertos[x].usuario_id)
                    .then(res3 => {
                        const nameaces=[...this.state.nameaces, res3.data.nombre];
                        this.setState({nameaces});

                   })
                }
            })

 
    }

  handleChange(date) {
        const filtrado = this.state.conciertos.filter(concierto => concierto.fecha_hora.substring(0,10) == date.format('YYYY-MM-DD'))
    const conciertos = this.state.conciertos
    this.setState({
      startDate: date,
      filtrado: filtrado
    });
  }


  render() {
    return (
      <div id="divisor">
        <div>
          <h2 id="buscatit"> Busar Evento por Fecha</h2>
        </div>
        <div>
          <DatePicker selected={this.state.startDate} onChange={this.handleChange}/>

          <ListaConciertos  nameace={this.state.nameaces} namereq={this.state.namereqs} conciertos={this.state.filtrado} />
        </div>
      </div>
    )
  }
}

export default LugarPorFecha;