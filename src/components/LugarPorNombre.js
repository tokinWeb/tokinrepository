import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { PropTypes } from 'react'
import { Component } from 'react'
import PageTemplate from './PageTemplate'
import '../css/react-datepicker.css';
import '../css/buscarLugar.css';
import ListaConciertos from './ListaConciertos.js';
import {Input, TextArea, GenericInput} from 'react-text-input'; // ES6
import axios from 'axios'

class LugarPorNombre extends React.Component {
constructor (props) {
    super(props)
    this.state = {

      filtrado: [],
      conciertos: [],
      namereqs: [],
      nameaces: [],
        lugar: ''
    };

    this.onChange = this.onChange.bind(this);

  }

      componentWillMount(){

             axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/eventsAllAll/" + localStorage.getItem('id'))
            .then(res => {
                this.setState({conciertos: res.data});

                for(let x=0; x<this.state.conciertos.length; x++){
                     axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/userd/" + this.state.conciertos[x].usuario_id_requester)
                    .then(res2 => {
                        const namereqs=[...this.state.namereqs, res2.data.nombre];
                        this.setState({namereqs});

                   })
                }

                for(let x=0; x<this.state.conciertos.length; x++){
                     axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/userd/" + this.state.conciertos[x].usuario_id)
                    .then(res3 => {
                        const nameaces=[...this.state.nameaces, res3.data.nombre];
                        this.setState({nameaces});

                   })
                }
            })

       
    }

  onChange(e) {   

    console.log(e.target.value)
    console.log(this.state.conciertos)


    if(e.target.value!==''){
      //const filtrado = this.state.conciertos.filter(concierto => concierto.lugar.toString().toLowerCase().replace(/\s/g," ").includes(e.target.value))
        const filtrado = this.state.conciertos.filter(namereqs => this.state.namereqs.toString().toLowerCase().replace(/\s/g," ").includes(e.target.value))
        console.log(this.state.namereqs)
        this.setState({filtrado})

    }else{
        this.setState({filtrado: []})
    }
    
  }

  render() {
    return (
      <div id="divisor">
        <div>
          <h2 id="buscatit"> Buscar Evento por Lugar</h2>
        </div>
        <div>
        <textarea className="buscar"
          onChange={this.onChange}/>

          <ListaConciertos nameace={this.state.nameaces} namereq={this.state.namereqs} conciertos={this.state.filtrado}  />
        </div>
      </div>
    )
  }
}


export default LugarPorNombre;