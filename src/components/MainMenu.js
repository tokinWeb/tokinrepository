import React from 'react';
import HomeIcon from 'react-icons/lib/fa/home'
import { NavLink } from 'react-router-dom'
import tokin from '../images/tokin.jpg'
import '../css/mainMenu.css';

const selectedStyle = { color: "white" };

export const MainMenu = () =>
    <nav className ="main-menu">
        <NavLink to ="/" style="font-size:30px;">  <img id="menutokin" src={tokin} alt="Tokin"></img></ NavLink >
        <NavLink to ="/controlPerfil" activeStyle ={ selectedStyle} > Perfil </NavLink>
        <NavLink to ="/LugarPorFecha" activeStyle ={ selectedStyle} > Evento por Fecha </NavLink>
        <NavLink to ="/LugarPorNombre" activeStyle ={ selectedStyle} > Evento por Nombre </NavLink>
        <NavLink to ="/controlSolicitudes" activeStyle ={ selectedStyle} > Solicitudes de Evento </NavLink>
        <NavLink to ="/controlMensajes" activeStyle ={ selectedStyle} > Mensajes </NavLink>
        <NavLink to ="/controlBares" activeStyle ={ selectedStyle} > Bares </NavLink>
        <NavLink to ="/controlBandas" activeStyle ={ selectedStyle} > Bandas </NavLink>
    </nav>;

export default MainMenu;