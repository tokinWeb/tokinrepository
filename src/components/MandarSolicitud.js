import React from 'react'

import band from '../images/logo-la-banda.png'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import TimePicker from 'react-bootstrap-time-picker';
import { Link } from 'react-router-dom';
import '../css/bandaDetalle.css';
import '../css/react-datepicker.css';
import axios from 'axios'

class MandarSolicitud extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showComponent: false,
      showSecond: true,
      str:'HH:mm:ss',
      startDate: moment(),
      eventDate:moment(),
      date: "1990-06-05",
      format: "YYYY-MM-DD",
      inputFormat: "DD/MM/YYYY",
      mode: "date",
      time: moment().get('hour')*3600,
      time2:(moment().get('hour')*3600)+1800,
      value:0
    };
    this.onButtonClick = this.onButtonClick.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onChange = this.onChange.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);
    this.handleTimeChange2 = this.handleTimeChange2.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }
  handleTimeChange(time) {
    console.log(time);     // <- prints "3600" if "01:00" is picked
    this.setState({ time });
  }
  handleTimeChange2(time2) {
    console.log(time2);     // <- prints "3600" if "01:00" is picked
    this.setState({ time2 });
  }
 
  onSelect(date) {
    this.setState({
      startDate:date
    });

}
  onChange(value) {
    this.setState({
      str: this.state.showSecond ? 'HH:mm:ss' : 'HH:mm',
    })
    
  console.log(value && value.format(this.state.str));
}

  onButtonClick() {
    console.log(this.state.time2-this.state.time);



        console.log(this.state.startDate.format('YYYY-MM-DD hh:mm:ss'))

       
    if(localStorage.getItem('tipo')==3){
         axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).post('http://localhost:8080/v1/event', {
        fecha_hora: this.state.startDate.format('YYYY-MM-DD hh:mm:ss'),
        usuario_id: localStorage.getItem('idr'),
        duracion: (this.state.time2-this.state.time)/60,
        usuario_id_requester: localStorage.getItem('id'),
        direccion_id:1
    })
        .then(function (response) {
        console.log(response);
         })
        .catch(function (error) {
        console.log(error);
        });
      }else{
         axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).post('http://localhost:8080/v1/event', {
        fecha_hora: this.state.startDate.format('YYYY-MM-DD hh:mm:ss'),
        usuario_id: localStorage.getItem('idm'),
        duracion: (this.state.time2-this.state.time)/60,
        usuario_id_requester: localStorage.getItem('id'),
        direccion_id:1
    })
        .then(function (response) {
        console.log(response);
         })
        .catch(function (error) {
        console.log(error);
        });
      }
      
  }

 render() {
    return (
    	<div className="homeSolicitud">
        <span className="label"> Elige Fecha</span>
          <DatePicker selected={this.state.startDate} onChange={this.handleChange}/>
        <span className="labelBusca"> Elige Hora</span>
          <div className="pickers">
            <TimePicker className="timePick" onChange={this.handleTimeChange} value={this.state.time}/>
            <TimePicker className="timePick2" onChange={this.handleTimeChange2} value={this.state.time2}/>
          </div>
        <div id="buttons5">
          <button className="button6" onClick={this.onButtonClick} >Solicitar</button>
        </div>
      </div>
    )
  }
}
export default MandarSolicitud