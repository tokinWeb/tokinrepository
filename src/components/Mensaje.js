import React from 'react'
import '../css/mensajes.css';
import { Link } from 'react-router-dom';

const Mensaje = ({id, name_e, date, texto, onRemove = () => {} }) =>
    <li>
        <span className="name">{name_e}</span>
        <span className="date">{date}</span>
        <span className="texto">{texto}</span>
        <button className="leer" id="mensajeb"><Link to ="/controlChat" style={{color:"white"}} >Leer</Link></button>
    </li>;
export default Mensaje