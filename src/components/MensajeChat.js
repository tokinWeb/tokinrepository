import React from 'react'
import { PropTypes } from 'react'
import { Component } from 'react'
import PageTemplate from './PageTemplate'
import axios from 'axios';
import '../css/mensajeChat.css';
import MensajeIndividual from './MensajeIndividual.js'
import ControlBares from './ControlBares.js'
import ControlBandas from './ControlBandas.js'

class MensajeChat extends Component {
    constructor(props){
        super(props);

        this.state = { id: localStorage.getItem('idm')};

        this.submit = this.submit.bind(this)

        texto:React.PropTypes.string
        usuario_id:React.PropTypes.int
        usuario_id_e:React.PropTypes.int

    }

    static propTypes = {
        onNewMessage:PropTypes.func.isRequired
    };


    static defaultProps = {
        onNewMessage: () => {}
    }    

    componentWillMount(){

         axios.create({
      headers:{'Authorization':'Basic ZGVtbzpzZWNyZXQ=','Content-type':'application/x-www-form-urlencoded'},
         }).get("http://localhost:8080/v1/user")
            .then(res => {
                this.setState({perfils: res.data});

            })
    }

    submit(e){
        e.preventDefault();
        const {_texto, _usuario_id, _usuario_id_e} = this.refs;
        this.props.onNewMessage(_texto.value, _usuario_id.value, _usuario_id_e.value);

        axios.post('http://localhost:8080/v1/mensaje', {

        texto:_texto.value,
        usuario_id:localStorage.getItem('idm'),
        usuario_id_e:localStorage.getItem('idr')

    })
        .then(function (response) {
        console.log(response);
         })
        .catch(function (error) {
        console.log(error);
        });

    }

    render(){
        return (

    <div className="chatbox">
		<form onSubmit={this.submit}>
        <div className="chatlogs">
                <MensajeIndividual/>
        </div>
            <div>                 
                <label for="usuario_id">
                    <input type="hidden" ref="_usuario_id" ></input> 
                </label>  
            </div>

            <div>                 
                <label for="usuario_id_e">
                    <input type="hidden" ref="_usuario_id_e" ></input> 
                </label>  
            </div>

        <div className="chat-form">                 
            <textarea type="text" ref="_texto" ></textarea> 
            <button name="submit" type="submit" id="submit" >Enviar</button> 
        </div>
    
    </form>

	</div>

        )
    }

}

export default MensajeChat;

