import React from 'react'
import { PropTypes } from 'react'
import { Component } from 'react'
import PageTemplate from './PageTemplate'
import axios from 'axios';
import '../css/mensajeChat.css';
import MensajeChat from './MensajeChat.js'
import ControlChat from './ControlChat.js'


class MensajeIndividual extends Component {
    constructor(props){
        super(props);

        texto:React.PropTypes.string
        photo:React.PropTypes.string
        usuario_id:React.PropTypes.int
        usuario_id_e:React.PropTypes.int

    }


	 static propTypes = {
	    texto:PropTypes.string.isRequired,
	    photo:PropTypes.string.isRequired
	 }

    render(){
    	if (this.props.usuario_id == this.props.usuario_id) {
	        return (
	        	<li>
		        <div className="chat self">
					<div className="user-photo"><img src={this.props.photo} /></div>
					<p className="chat-message">{this.props.texto}</p>
				</div>
				</li>
	        )
    	} else{
	        return (
	        	<li>
				<div className="chat friend">
					<div className="user-photo"><img src={this.props.photo} /></div>
					<p className="chat-message">{this.props.texto}</p>
				</div>
				</li>
	        )
    	}

    }

}

export default MensajeIndividual;
