import React from 'react'
import '../css/perfil.css';
import ControlPerfil from './ControlPerfil.js'


const Perfil = ({id, uuid, nombre, genero, correo, tel_movil, tipo, password, estatus, imagen, direccion_id, fecha_registro, fecha_act, onClick= () => {} }) =>
    <div className="perfil">
      <div id="datosPerfil">
          <img alt={imagen} src={imagen}/>
          <span className="nombre">Nombre: {nombre}</span>
          <span className="tel_movil">Teléfono: {tel_movil}</span>
          <span className="correo">Correo: {correo}</span>
          <span className="genero">Género: {genero}</span>
      </div>
    </div>

Perfil.propTypes = {
  imagen:React.PropTypes.string.isRequired,
  nombre:React.PropTypes.string.isRequired,
  tel_movil:React.PropTypes.string.isRequired,
  correo:React.PropTypes.string.isRequired,
  genero:React.PropTypes.string.isRequired
}

Perfil.defaultProps = {
  imagen:"[imagen]",
  nombre:"[nombre]",
  tel_movil:"[tel_movil]",
  correo:"[correo]",
  genero:"[genero]"
}

export default Perfil;