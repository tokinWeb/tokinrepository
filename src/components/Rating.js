import { Component } from 'react'
import { PropTypes } from 'react'
import React from 'react';
import PageTemplate from './PageTemplate'
import '../css/starRating.css';

class Rating extends Component {

    render(){
        return (
        <form onSubmit={this.submit}>
        <div className="star-rating">
            <input id="star-1" type="radio" name="rating" value="1" ref="starsValue" ></input>
            <label for="star-1" title="1 stars">
                    <i className="active fa fa-star" aria-hidden="true" ></i>
            </label>
            <input id="star-2" type="radio" name="rating" value="2" ref="starsValue" ></input>
            <label for="star-2" title="2 stars">
                    <i className="active fa fa-star" aria-hidden="true"></i>
            </label>
            <input id="star-3" type="radio" name="rating" value="3" ref="starsValue" ></input>
            <label for="star-3" title="3 stars">
                    <i className="active fa fa-star" aria-hidden="true"></i>
            </label>
            <input id="star-4" type="radio" name="rating" value="4" ref="starsValue" ></input>
            <label for="star-4" title="4 stars">
                    <i className="active fa fa-star" aria-hidden="true"></i>
            </label>
            <input checked id="star-5" type="radio" name="rating" value="5" ref="starsValue" ></input>
            <label for="star-5" title="5 star">
                    <i className="active fa fa-star" aria-hidden="true"></i>
            </label>
        </div>
        </form>
        );
    }
}

export default Rating;