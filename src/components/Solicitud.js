import React from 'react'

const Solicitud = ({name, aceptar, rechazar, id, uuid, fecha_hora, fecha_creacion, duracion, usuario_id_b, usuario_id_r, usuario_id_requester, usuario_id, tipo, direccion_id, estatus = () => {} }) =>
   
    <li>
        <span className="name">{name}</span>
        <span className="date">{fecha_hora}</span>
        <a className="rechazar" onClick={() => rechazar({uuid})}>
        	Rechazar
        </a> 
        <a className="aceptar" onClick={() => aceptar({uuid})}>
        	Aceptar
        </a>
    </li>;
export default Solicitud